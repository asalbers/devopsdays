require 'spec_helper'

describe 'devopsdays' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          fix_cgi: false,
          php_extensions: {},
          fpm_listen_owner: 'foo',
          fpm_listen_group: 'bar',
          fpm_user: 'baz',
          fpm_group: 'foobar',
          fpm_skt_dir: '/foo/bar/baz.sock',
        }
      end

      it { is_expected.to compile }
    end
  end
end
